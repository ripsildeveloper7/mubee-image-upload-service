var bannerDA = require('./bannerDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

exports.uploadImageBanner = function (req, res) {
    try {
      const params = {
        Bucket: env.homebucket + '/' + 'images' + '/' + 'banners' + '/' + req.params.id, // create a folder and save the image
        Key: req.file.originalname,
        Body: req.file.buffer,
      };
      s3.upload(params, function (err, data) {
        if (err) {
          console.log(err);
        } else {
           /*  bannerDA.uploadImageBanner(req, params, res); */
           res.status(200).json(params);
        }
      });
    } catch (error) {
      console.log(error);
    }
    } 

    exports.updateBannerImage = function (req, res) {
        try {
          const params = {
            Bucket: env.homebucket + '/' + 'images' + '/' + 'banners' + '/' + req.params.id, // create a folder and save the image
            Key: req.file.originalname,
            Body: req.file.buffer,
          };
          s3.upload(params, function (err, data) {
            if (err) {
              console.log(err);
            } else {
                /* bannerDA.updateBannerImage(req, params, res); */
                res.status(200).json(params);
            }
          });
        } catch (error) {
          console.log(error);
        }
        }
        
        exports.uploadBaseSingleImage = function (req, res) {
          try {
            bannerDA.uploadBaseSingleImage(req, res);
          } catch (error) {
            console.log(error);
          }
        }
        exports.uploadBase64PromotionBanner = function (req, res) {
          try {
            bannerDA.uploadBase64PromotionBanner(req, res);
          } catch (error) {
            console.log(error);
          }
        }