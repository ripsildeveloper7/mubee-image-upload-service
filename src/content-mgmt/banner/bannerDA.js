 var bannerDetails = require('../../model/banners.model');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');
var s3 = require('../../config/s3.config');

/*
exports.uploadImageBanner = function (req, file, res) {
    bannerDetails.findOne({
        '_id': req.params.id
    }).select().exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            data.bannerImageName = file.Key;
            data.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}

exports.updateBannerImage = function (req, file, res) {
    bannerDetails.findOne({
        '_id': req.params.id,
    }, function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (data.bannerImageName === file.Key) {
                res.status(200).json(data);
            } else {
                var s3 = new AWS.S3();
                s3.deleteObject({
                    Bucket: env.homebucket,
                    Key: 'images' + '/' + 'banners' + '/' + req.params.id + '/' + data.bannerImageName
                }, function (err, data) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        bannerDetails.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, data1) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                data1.bannerImageName = file.Key;
                                data1.save(function (err, data) {
                                    if (err) {
                                        res.status(500).json(err);
                                    } else {
                                        data.bannerImageName = env.HomeImageServerPath + 'banners' + '/' + data._id + '/' + data.bannerImageName;
                                        res.status(200).json(data);
                                    }
                                })
                            }
                        })
                    }
                })
            }

        }
    });
} */

exports.uploadBaseSingleImage = function (req, res) {
    const base64Data = Buffer.from(req.body.bannerImageName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const type = req.body.bannerImageName.split(';')[0].split('/')[1]
    const params = {
      Bucket: env.homebucket + '/' + 'images' + '/' + 'banners' + '/' + req.params.id, // create a folder and save the image
      Key: req.body.bannerName,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      Body: base64Data,
      ContentType: `image/${type}`
    };
  
    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        res.status(200).json(data);
      }
    });
  }

  exports.uploadBase64PromotionBanner = function (req, res) {
    const base64Data = Buffer.from(req.body.base64Image.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const type = req.body.base64Image.split(';')[0].split('/')[1]
    const params = {
      Bucket: env.homebucket + '/' + 'images' + '/' + 'promotionbanner' + '/' + req.params.id, // create a folder and save the image
      Key: req.body.imageName,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      Body: base64Data,
      ContentType: `image/${type}`
    };
  
    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        res.status(200).json(data);
      }
    });
  }