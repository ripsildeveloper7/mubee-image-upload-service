'use strict';
var headerMgr = require('./header/headerMgr');
var adsMgr = require('./ads/adsMgr');
var bannerMgr = require('./banner/bannerMgr');
var upload = require('../config/multer.config');
var footerMgr  = require('./footer/footerMgr');

module.exports = function (app) {
  app.route('/createheaderimage')                 // ADD or Update Header Logo
    .put(upload.single('single'), headerMgr.uploadImageMulter);

  app.route('/uploadadsimage/:id')                // ADD ADS Image
    .put(upload.single('single'), adsMgr.uploadImageADS);
    
  app.route('/updateadsimage/:id')                // Update ADS Image
    .put(upload.single('single'), adsMgr.updateADSImage);  

  app.route('/uploadbannerimage/:id')             // ADD Banner Image
    .put(upload.single('single'), bannerMgr.uploadImageBanner);    

  app.route('/updatebannerimage/:id')             // Upload Banner Image
    .put(upload.single('single'), bannerMgr.updateBannerImage);    
  app.route('/uploadfooterimage/:id')             // Upload footer Image
    .put(upload.single('single'), footerMgr.uploadFooterImageMulter);      
    
    // ads image upload

    app.route('/adsbase64imagesingle/:id')
    .put(adsMgr.uploadAdsBaseSingleImage);
     // image upload using base64
     app.route('/base64imagesingle/:id')
     .put(bannerMgr.uploadBaseSingleImage);
     app.route('/base64promotionbanner/:id')
     .put(bannerMgr.uploadBase64PromotionBanner);
     app.route('/base64header/:id')
     .put(headerMgr.uploadBase64Header);
}