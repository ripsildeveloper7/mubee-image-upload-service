var headerDA = require('./headerDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

exports.uploadImageMulter = function (req, res) {
  try {
    const params = {
      Bucket: env.homebucket + '/' + 'images' + '/' + 'header', // create a folder and save the image
      Key: req.file.originalname,
      Body: req.file.buffer,
    };

    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
      
        res.status(200).json(params);
      }
    });
  } catch (error) {
    console.log(error);
  }
  } 
  exports.uploadBase64Header = function(req, res) {
    try {
      headerDA.uploadBase64Header(req, res);
    } catch (error) {
      console.log(error);
    }
  }