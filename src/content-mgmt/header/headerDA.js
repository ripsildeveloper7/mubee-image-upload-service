var headerDA = require('./headerDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

/* var headerDetail = require('../../model/header.model');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

exports.uploadImageMulter = function (req, file, res) {
    headerDetail.find({}).select().exec(function (err, headerData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var header = new headerDetail();
            header.logoImageName = file.Key;
            if (headerData.length === 0) {
                header.save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while saving data'
                        })
                    } else {
                        headerDetail.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    message: "Some error occurred while retrieving notes."
                                });
                            } else {
                                data[0].logoImageName = env.HomeImageServerPath + 'header' + '/' + data[0].logoImageName;
                                res.status(200).json(data);
                            }
                        });
                    }
                })

            } else {
                if (headerData[0].logoImageName === file.Key) {
                    res.status(200).json(headerData);
                } else {
                    var s3 = new AWS.S3();
                    s3.deleteObject({
                        Bucket: env.homebucket,
                        Key: 'images' + '/' + 'header' + '/' + headerData[0].logoImageName
                    }, function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            headerDetail.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    data[0].logoImageName = file.Key;
                                    data[0].save(function (err, data) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(data);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }

            }
        }
    });
} */

exports.uploadBase64Header = function (req, res) {
    const base64Data = Buffer.from(req.body.base64Image.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const type = req.body.base64Image.split(';')[0].split('/')[1]
    const params = {
      Bucket: env.homebucket + '/' + 'images' + '/' + 'header' + '/' + req.params.id, // create a folder and save the image
      Key: req.body.imageName,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      Body: base64Data,
      ContentType: `image/${type}`
    };
  
    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        res.status(200).json(data);
      }
    });
  }