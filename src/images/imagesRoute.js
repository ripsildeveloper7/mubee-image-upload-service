'use strict';
var imagesMgr = require('./imagesMgr');
var upload = require('./../config/multer.config');

module.exports = function (app) {
  app.route('/imagesingle/:id')
    .put(imagesMgr.uploadSingleImage);
  app.route('/image/:id')
    .put(upload.single('single'), imagesMgr.uploadSingleImageMulter);

  app.route('/multiimage/:id')
  .put(upload.array('uploads[]'), imagesMgr.uploadMultiImageMulter);
}
