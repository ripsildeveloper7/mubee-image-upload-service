var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');


exports.uploadImageSizeGuide = function (req, res) {
  const base64Data = Buffer.from(req.body.uploadedImage.replace(/^data:image\/\w+;base64,/, ""), 'base64');
  const type = req.body.uploadedImage.split(';')[0].split('/')[1]
  const params = {
    Bucket: env.ProductBucket + '/' + 'images' + '/' + 'size' + '/' + req.params.id, // create a folder and save the image
    Key: req.body.imageName,
    ACL: 'public-read',
    ContentEncoding: 'base64',
    Body: base64Data,
    ContentType: `image/${type}`
  };

  s3.upload(params, function (err, data) {
    if (err) {
      console.log(err);
    } else {
      res.status(200).json(data);
    }
  });
    } 
