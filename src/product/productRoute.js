'use strict';
var productImageMgr = require('./productimage/productImageMgr');
var categoryMgr = require('./categoryimage/categoryMgr');
var brandMgr = require('./brandimage/brandMgr');
var upload = require('../config/multer.config');
var sizeGuide = require('./productSizeGuide/productSizeGuideMgr');

module.exports = function (app) {
  /* app.route('/imagesingle/:id')
    .put(productImageMgr.uploadSingleImage); */
  /* app.route('/productimage/:id')
    .put(upload.single('single'), productImageMgr.uploadSingleImageMulter);
    
 */
  app.route('/productimagesthree/:id')
    .put(upload.array('uploads[]'), productImageMgr.uploadMultiImageMulter);
  app.route('/editproductimagesthree/:id') // edit product image
    .put(upload.array("uploads[]"), productImageMgr.uploadMultiImageEdit);
    app.route('/brandimagesthree/:id')
    .put(upload.single('single'), brandMgr.uploadBrandImages); // Upload Brand Image
// app.route('/supercategoryimagesthree/:id')
//     .put(upload.single('single'), categoryMgr.uploadSuperCategoryImages); // Upload Super Category

    app.route('/maincategoryimagesthree/:mainId')
    .put(categoryMgr.addMainCategoryImage64); // Upload Main 
    
    app.route('/supercategoryimagesthree/:id')
    .put(categoryMgr.addSuperCategoryImage64); // Upload Super 
    app.route('/subcategoryimagesthree/:subId')
    .put(categoryMgr.addSubCategoryImage64); // Upload Sub Category Image 
    
    app.route('/sizeimage/:id')
    .put(upload.single('single'), sizeGuide.uploadImageSizeGuide); // Upload Size Guide

    app.route('/uploadcateogorybanner/:id')
    .post(upload.single('single'), categoryMgr.uploadCategoryBannerImage); // Upload  Category Banner Image 

}
