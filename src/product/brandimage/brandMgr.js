var brandDA = require('./brandDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');


exports.uploadBrandImages = function (req, res) {
    try {
      const params = {
        Bucket: env.ProductBucket + '/' + 'images' + '/' + 'about' + '/' + req.params.id, // create a folder and save the image
        Key: req.file.originalname,
        ACL: 'public-read',
        Body: req.file.buffer,
      };
  
      s3.upload(params, function (err, data) {
        if (err) {
          console.log(err);
        } else {
            brandDA.uploadBrandImages(req, req.file, res);
        }
      });
    } catch (error) {
      console.log(error);
    }
  }