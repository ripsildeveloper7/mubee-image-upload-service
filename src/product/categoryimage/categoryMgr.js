var categoryDA = require('./categoryDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');


exports.uploadSuperCategoryImages = function (req, res) {
  try {
    const params = {
      Bucket: env.ProductBucket + '/' + 'images' + '/' + 'category' + '/' + req.params.id, // create a folder and save the image
      Key: req.file.originalname,
      ACL: 'public-read',
      Body: req.file.buffer,
    };

    s3.upload(params, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        categoryDA.uploadSuperCategoryImages(req, req.file, res);
      }
    });
  } catch (error) {
    console.log(error);
  }
}

// exports.addMainCategoryImage = function (req, res) {
//   try {
//     const params = {
//       Bucket: env.ProductBucket + '/' + 'images' + '/' + 'maincategory' + '/' + req.params.mainId, // create a folder and save the image
//       Key: req.file.originalname,
//       ACL: 'public-read',
//       Body: req.file.buffer,
//     };
//       s3.upload(params, (err, data) => {
//           if (err) {
//             console.log(err);
//           } else {
//             categoryDA.addMainCategoryImage(req,req.file,res);
//           }
//       });
   
//   } catch (error) {
//       console.log(error);
//   }
// }

exports.addSuperCategoryImage64 = function (req, res) {
  try {


    categoryDA.addSuperCategoryImage64(req, res);


  } catch (error) {
    console.log(error);
  }
}

exports.addMainCategoryImage64 = function (req, res) {
  try {


    categoryDA.addMainCategoryImage64(req, res);


  } catch (error) {
    console.log(error);
  }
}

exports.addSubCategoryImage64 = function (req, res) {
  try {


    categoryDA.addSubCategoryImage64(req, res);


  } catch (error) {
    console.log(error);
  }
}


// exports.addSubCategoryImage = function (req, res) {
//   try {
//     const params = {
//       Bucket: env.ProductBucket + '/' + 'images' + '/' + 'subcategory' + '/' + req.params.subId, // create a folder and save the image
//       Key: req.file.originalname,
//       ACL: 'public-read',
//       Body: req.file.buffer,
//     };
//       s3.upload(params, (err, data) => {
//           if (err) {
//             console.log(err);
//           } else {
//             categoryDA.addSubCategoryImage(req, req.file,res);
//           }
//       });
//   } catch (error) {
//       console.log(error);
//   }
// }

exports.uploadCategoryBannerImage = function (req, res) {
  try {
    const params = {
      Bucket: env.ProductBucket + '/' + 'images' + '/' + 'categorybanner' + '/' + req.params.id, // create a folder and save the image
      Key: req.file.originalname,
      ACL: 'public-read',
      Body: req.file.buffer,
    };
      s3.upload(params, (err, data) => {
          if (err) {
            console.log(err);
          } else {
            res.status(200).json(params);
          }
      });
  } catch (error) {
      console.log(error);
  }
}
