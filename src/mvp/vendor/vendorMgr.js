var vendorDA = require('./vendorDA');
var s3 = require('../../config/s3.config');
var env = require('../../config/s3.env');
const AWS = require('aws-sdk');

exports.uploadImageChancelCheque = function (req, res) {
    try {
        const params = {
            Bucket: env.vendorbucket + '/' + 'images' + '/' + 'vendor' + '/' + req.params.id + '/' + 'cancelcheque', // create a folder and save the image
            Key: req.file.originalname,
            Body: req.file.buffer,
        };
        s3.upload(params, function (err, data) {
            if (err) {
                console.log(err);
            } else {
                /* vendorDA.uploadImageCancelCheque(req, params, res); */
                res.status(200).json(params);
            }
        });
    } catch (error) {
        console.log(error);
    }
}
/* exports.updateImageCancelCheque = function (req, res) {
    try {
        const params = {
            Bucket: env.vendorbucket + '/' + 'images' + '/' + 'vendor' + '/' + req.params.id + '/' + 'cancelcheque', // create a folder and save the image
            Key: req.file.originalname,
            Body: req.file.buffer,
        };
        s3.upload(params, function (err, data) {
            if (err) {
                console.log(err);
            } else {
                vendorDA.updateImageCancelCheque(req, params, res);
            }
        });
    } catch (error) {
        console.log(error);
    }
} */

exports.uploadImageDigitalSignature = function (req, res) {
    try {
        const params = {
            Bucket: env.vendorbucket + '/' + 'images' + '/' + 'vendor' + '/' + req.params.id + '/' + 'digitalsignature', // create a folder and save the image
            Key: req.file.originalname,
            Body: req.file.buffer,
        };
        s3.upload(params, function (err, data) {
            if (err) {
                console.log(err);
            } else {
                /* vendorDA.uploadImageDigitalSignature(req, params, res); */
                res.status(200).json(params);
            }
        });
    } catch (error) {
        console.log(error);
    }
}
/* exports.updateImageDigitalSignature = function (req, res) {
    try {
        const params = {
            Bucket: env.vendorbucket + '/' + 'images' + '/' + 'vendor' + '/' + req.params.id + '/' + 'digitalsignature', // create a folder and save the image
            Key: req.file.originalname,
            Body: req.file.buffer,
        };
        s3.upload(params, function (err, data) {
            if (err) {
                console.log(err);
            } else {
                vendorDA.updateImageDigitalSignature(req, params, res);
            }
        });
    } catch (error) {
        console.log(error);
    }
} */