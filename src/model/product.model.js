var mongoose = require('mongoose');
var size = require('./size.model');

const ProductSchema = new mongoose.Schema({
  /* productId: String, */
  productName: String,
  productDescription: String,
  /* sku: String, */
  /*  upc: String,
   ean: String,
   jan: String,
   isbn: String,
   mpn: String, */
  styleCode: String,
  quantity: Number,
  /* manufacturerId: String, */
  productImageName: [String],
  manufacturer: String,
  price: Number,
 /*   special: String,
  reward: String, */
  bulletPoints: String,
  height: String,
  weight: String,
  material: String,
  occassion: String,
  color: String,
  tags: [{type: mongoose.Schema.Types.ObjectId, ref: 'productTagModelSchema' }],
  /* taxId: String,
  rating: Number,
  reviews: String,
  status: Boolean, */
  // category map
  superCategoryId: {type: mongoose.Schema.Types.ObjectId, ref: 'SuperCategorySchema' },
  mainCategoryId: mongoose.Schema.Types.ObjectId,
  subCategoryId: String,

  // meta tag start
  metaTitle: String,
  metaDescription: String,
  metaKeyword: String,
 
  // meta tag end
  dateAdded: Date,
  dateModified: Date,
  brandId: {type: mongoose.Schema.Types.ObjectId, ref: 'BrandSchema' } ,
  sizeVariantId: String,
  productVariant: [size],
  publish: Boolean
  
});

const product = mongoose.model('product', ProductSchema);
module.exports = product;