var mongoose = require('mongoose');


const SizeGuideSchema = new mongoose.Schema({
    title: String,
    imageName: [String],
});


const SizeGuide = mongoose.model('sizeguide', SizeGuideSchema);
module.exports = SizeGuide;
